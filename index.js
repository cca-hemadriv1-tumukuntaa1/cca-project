const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
//app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
const cors=require('cors')
app.use(cors())
app.listen(port,()=>
    console.log('Express is running on port'+port))
app.get('/',(req,res)=>{
    res.send('Microservice Gateway by Vinay Varma Hemadri & Abhinava K. Tumukunta');
})
app.get('/microservice1',function(req,res) {
    var result1 = new Date().getFullYear() - req.query.data
    var result2 = null
    if(req.query.data % 4 == 0){
        result2 = "a Leap Year"
    }
    else{
        result2 = "Not a Leap Year"
    }
    res.send("Your age is: "+result1+", And your year of birth is "+result2)
})